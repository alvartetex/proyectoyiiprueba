<?php

namespace app\controllers;

use app\models\equipo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EquipoController implements the CRUD actions for equipo model.
 */
class EquipoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all equipo models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => equipo::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'nomequipo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single equipo model.
     * @param string $nomequipo Nomequipo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nomequipo)
    {
        return $this->render('view', [
            'model' => $this->findModel($nomequipo),
        ]);
    }

    /**
     * Creates a new equipo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new equipo();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing equipo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nomequipo Nomequipo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nomequipo)
    {
        $model = $this->findModel($nomequipo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing equipo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nomequipo Nomequipo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nomequipo)
    {
        $this->findModel($nomequipo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the equipo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nomequipo Nomequipo
     * @return equipo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nomequipo)
    {
        if (($model = equipo::findOne(['nomequipo' => $nomequipo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
